package mme.format.svgpath;

import mme.format.svgpath.Data;

using StringTools;


class Reader {

    var _d : String;
    public var pathData ( default, null ) : Array<String>;

    var _pathCommands = 'MmZzLlHhVvCcSsQqTtAa';
    // https://www.w3.org/TR/SVG/paths.html#PathDataBNF
    var _pathDataRegex : EReg = ~/[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|\s*[MmZzLlHhVvCcSsQqTtAa]\s*/g;

    public function new( d : String ) {
        _d = d == null ? '' : d;
    }

    public function read() : Data {

        pathData = collectMatches();

        var elements : Array<PathElement> = [];

        var command : String = '';
        var coordinatesAreRelative = false;
        var pathDataSource = new PathDataSource( pathData );

        while( pathDataSource.hasNext() ) {

            var currentData = pathDataSource.peek();

            if( currentData.length == 1 && _pathCommands.indexOf( currentData ) != -1 ) {

                // it's a path command
                command = currentData;
                pathDataSource.consume();

                coordinatesAreRelative = command.indexOf( command.toUpperCase() ) == -1;
                command = command.toUpperCase();
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands
            if( command == 'M' ) {

                // currentPoint = x,y
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CMoveTo( x, y, coordinatesAreRelative ) );

                // If there happens to be more coordinates after initial 'M'
                // they are treated as LineTo commands. We set 'command' here
                // and the while loop will fallthrough to the LineTo handling code
                // if current data is not a command. If it is, 'command' will be
                // set to whatever the current command might be.
                command = 'L';
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataLinetoCommands
            else if( command == 'L' ) {

                // currentPoint = Start point - P0

                // End point - P1
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CLineTo( x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataLinetoCommands
            else if( command == 'H' ) {

                // currentPoint = Start point - P0

                // End point - P1.y = P0.y
                // End point - P1.x
                var x = pathDataSource.consumeFloat();

                elements.push( CLineToHorizontal( x, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataLinetoCommands
            else if( command == 'V' ) {

                // currentPoint = Start point - P0

                // End point - P1.x = P0.x
                // End point - P1.y
                var y = pathDataSource.consumeFloat();

                elements.push( CLineToVertical( y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataQuadraticBezierCommands
            else if( command == 'Q' ) {

                // currentPoint = Start point - P0

                // prevControlPoint = Control point - P1
                var x1 = pathDataSource.consumeFloat();
                var y1 = pathDataSource.consumeFloat();

                // End point - P2
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CQuadraticBezier( x1, y1, x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataQuadraticBezierCommands
            else if( command == 'T' ) {

                // currentPoint = Start point - P0

                // prevControlPoint = Control point - P1

                // End point - P2
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CQuadraticBezierSmooth( x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands
            else if( command == 'C' ) {

                // currentPoint = Start point - P0

                // Control point - P1
                var x1 = pathDataSource.consumeFloat();
                var y1 = pathDataSource.consumeFloat();

                // Control point - P2
                var x2 = pathDataSource.consumeFloat();
                var y2 = pathDataSource.consumeFloat();

                // End point - P3
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CCubicBezier( x1, y1, x2, y2, x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands
            else if( command == 'S' ) {

                // currentPoint = Start point - P0

                // prevControlPoint = Control point - P1

                // Control point - P2
                var x2 = pathDataSource.consumeFloat();
                var y2 = pathDataSource.consumeFloat();

                // End point - P3
                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CCubicBezierSmooth( x2, y2, x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
            // https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
            else if( command == 'A' ) {

                // Parameters:
                // (rx ry x-axis-rotation large-arc-flag sweep-flag x y)+
                var rx = pathDataSource.consumeFloat();
                var ry = pathDataSource.consumeFloat();

                var xAxisRotation = pathDataSource.consumeFloat();
                var fA = pathDataSource.consumeInt();
                var fS = pathDataSource.consumeInt();

                var x = pathDataSource.consumeFloat();
                var y = pathDataSource.consumeFloat();

                elements.push( CEllipticArc( rx, ry, xAxisRotation, fA, fS, x, y, coordinatesAreRelative ) );
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataClosePathCommand
            else if( command == 'Z' ) {

                elements.push( CClosePath );
            }
            
            else {

                // ;
            }
        }

        return {
            elements: elements
        }
    }

    function collectMatches() : Array<String> {

        var pathData = [];

        var str = _d;
        while( _pathDataRegex.match( str ) ) {

            str = _pathDataRegex.matchedRight();
            var data = _pathDataRegex.matched(0).trim();

            pathData.push( data );
        }

        return pathData;
    }
}


class PathDataSource {
    var _pathData : Array<String>;
    var idx : Int;
    public function new( pathData : Array<String> ) {
        _pathData = pathData;
        idx = 0;
    }

    public function hasNext() : Bool {
        return idx < _pathData.length;
    }

    public function next() : String {
        var value = _pathData[ idx ];
        idx++;
        return value;
    }

    public function peek() : String {
        return _pathData[ idx ];
    }

    public function consume() : Void {
        next();
    }

    public function consumeFloat() : Float {
        return Std.parseFloat( next() );
    }

    public function consumeInt() : Int {
        return Std.parseInt( next() );
    }
}
