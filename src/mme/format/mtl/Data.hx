package mme.format.mtl;

typedef RGB = {

    var r : Float;
    var g : Float;
    var b : Float;
}

typedef XYZ = {

    var x : Float;
    var y : Float;
    var z : Float;
}

enum Line {

    CEnd;

    CKa( ka : RGB );
    CKaxyz( ka : XYZ );
    CKd( ka : RGB );
    CKdxyz( ka : XYZ );
    CKs( ka : RGB );
    CKsxyz( ka : XYZ );
    CKe( ka : RGB );
    CKexyz( ka : XYZ );
    CNs( ns : Float );
    Cd( d : Float );
    CIllumination( n : Int );
    CMaterial( newmtl : String );

    CUnknown( l : String );
}

typedef Data = Array< Line >;
