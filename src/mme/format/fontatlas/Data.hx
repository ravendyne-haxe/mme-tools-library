package mme.format.fontatlas;

import haxe.io.Bytes;

typedef GlyphData = {
    var point : String;
    var xpos : Int;
    var ypos : Int;
    // glyph dimensions
    var width : Float;
    var height : Float;
    // horizontal layout metrics
    var advanceX : Float;
    var horizontalBearingX : Float;
    var horizontalBearingY : Float;
    // vertical layout metrics
    var advanceY : Float;
    var verticalBearingX : Float;
    var verticalBearingY : Float;
}

typedef Header = {
    var magic : String;
    var version : Int;
}

typedef Data = {
    var fontSize : Float;
    var glyphs : Array<GlyphData>;
    var atlas : Bytes;
    var atlasWidth : Int;
    var atlasHeight : Int;
}
