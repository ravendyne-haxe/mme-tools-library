package mme.format.fontatlas;

import mme.format.fontatlas.Data;
import mme.format.fontatlas.Writer;


class Reader {

    public static final fingerprint = "fontatlas";

	var i : haxe.io.Input;

	public function new(i) {
		this.i = i;
	}

/*
    // header
    UINT8[9] – Magic Number
    INT32 – File format version

    // body
    REAL32 – Rendered font size
    INT32 – Number of glyphs

    foreach glyph
        INT32 – point (character)
        INT32 – x position in atlas image
        INT32 – y position in atlas image
        REAL32 – width
        REAL32 – height
        REAL32 – advance x
        REAL32 – advance y
        REAL32 – horizontal layout bearing x
        REAL32 – horizontal layout bearing y
        REAL32 – vertical layout bearing x
        REAL32 – vertical layout bearing y
    end

    INT32 - atlas image width
    INT32 - atlas image height
    INT32 – Size of atlas image data in bytes
    UINT8[...] - atlas image data
*/

	public function read() : Data {

        i.bigEndian = false;

        var header = readHeader();

        var fontSize = i.readFloat();

        if( header.magic != fingerprint ) {
            throw "This doesn't seem to be fontatlas file.";
        }
        if( header.version != Writer.version ) {
            throw "Wrong fontatlas file format version.";
        }

        // read glyphs
        var glyphs : Array<GlyphData> = [];

        var numberOfGlyphs = i.readInt32();

        for( idx in 0...numberOfGlyphs ) {

            glyphs.push( readGlyphData() );
        }

        // read atlas image data

        var atlasWidth = i.readInt32();
        var atlasHeight = i.readInt32();
        var atlasDataLength = i.readInt32();
        var imgBytes = haxe.io.Bytes.alloc( atlasDataLength );
        i.readFullBytes( imgBytes, 0, atlasDataLength );

        return {
            fontSize: fontSize,
            glyphs: glyphs,
            atlas: imgBytes,
            atlasWidth: atlasWidth,
            atlasHeight: atlasHeight,
        }
    }

    function readHeader() : Header {

        var magicBytes = i.read( fingerprint.length );
        var magic = magicBytes.getString( 0, magicBytes.length );
        var version = i.readInt32();

        return {
            magic : magic,
            version : version,
        };
    }

    function readGlyphData() : GlyphData {

        var pointInt = i.readInt32();
        var point = String.fromCharCode( pointInt );

        var xpos = i.readInt32();
        var ypos = i.readInt32();

        var width = i.readFloat();
        var height = i.readFloat();

        var advanceX = i.readFloat();
        var advanceY = i.readFloat();
        var horizontalBearingX = i.readFloat();
        var horizontalBearingY = i.readFloat();
        var verticalBearingX = i.readFloat();
        var verticalBearingY = i.readFloat();

        return {
            xpos: xpos,
            ypos: ypos,
            point: point,
            width: width,
            height: height,
            advanceX: advanceX,
            advanceY: advanceY,
            horizontalBearingX: horizontalBearingX,
            horizontalBearingY: horizontalBearingY,
            verticalBearingX: verticalBearingX,
            verticalBearingY: verticalBearingY,
        }
    }
}
