package mme.format.fontatlas;

import mme.format.fontatlas.Data;

class Writer {

	var o : haxe.io.Output;
    public static var version ( default, never ) = 0x00010100; // v 1.1.0

	public function new(o) {
		this.o = o;
	}

    public function write( data : Data ) {

        // magic thingie at the start
        writeHeader();

        o.writeFloat( data.fontSize );

        // glyphs
        o.writeInt32( data.glyphs.length );
        for( glyph in data.glyphs ) {
            writeGlyphData( glyph );
        }

        // atlas image
        writeAtlasImage( data );
    }

    function writeHeader() {

        var magicThingie = haxe.io.Bytes.ofString( Reader.fingerprint );
        o.writeBytes( magicThingie, 0, magicThingie.length );
        o.writeInt32( version );
    }

    function writeGlyphData( data : GlyphData ) {

        o.writeInt32( data.point.charCodeAt(0) );

        o.writeInt32( data.xpos );
        o.writeInt32( data.ypos );

        o.writeFloat( data.width );
        o.writeFloat( data.height );

        o.writeFloat( data.advanceX );
        o.writeFloat( data.advanceY );
        o.writeFloat( data.horizontalBearingX );
        o.writeFloat( data.horizontalBearingY );
        o.writeFloat( data.verticalBearingX );
        o.writeFloat( data.verticalBearingY );
    }

    function writeAtlasImage( data : Data ) {

        o.writeInt32( data.atlasWidth );
        o.writeInt32( data.atlasHeight );

        o.writeInt32( data.atlas.length );
        o.writeFullBytes( data.atlas, 0, data.atlas.length );
    }
}
