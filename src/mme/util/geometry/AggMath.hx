package mme.util.geometry;

import mme.math.glmatrix.Vec2;


//-------------------------------------------------------------
//
// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_math.h
//
//-------------------------------------------------------------

class AggMath {
    //-------------------------------------------------------------
    public static var intersection_epsilon : Float = 1.0e-30;

    //-------------------------------------------------------------
    inline public static function calc_intersection(ax : Float, ay : Float, bx : Float, by : Float,
                                      cx : Float, cy : Float, dx : Float, dy : Float,
                                      out : Vec2) : Bool
    {
        var num : Float = (ay-cy) * (dx-cx) - (ax-cx) * (dy-cy);
        var den : Float = (bx-ax) * (dy-cy) - (by-ay) * (dx-cx);

        if(Math.abs(den) < intersection_epsilon) return false;

        var r : Float = num / den;
        out.x = ax + r * (bx-ax);
        out.y = ay + r * (by-ay);

        return true;
    }

    //-------------------------------------------------------------
    inline public static function calc_distance(x1 : Float, y1 : Float, x2 : Float, y2 : Float) : Float
    {
        var dx : Float = x2-x1;
        var dy : Float = y2-y1;
        return Math.sqrt(dx * dx + dy * dy);
    }

    //-------------------------------------------------------------
    inline public static function calc_sq_distance(x1 : Float, y1 : Float, x2 : Float, y2 : Float) : Float
    {
        var dx : Float = x2-x1;
        var dy : Float = y2-y1;
        return dx * dx + dy * dy;
    }
}


//-------------------------------------------------------------
//
// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_math_stroke.h
//
//-------------------------------------------------------------

typedef VertexConsumer = {
    function add( x : Float, y : Float ) : Void;
    function remove_all() : Void;
}

class SimpleVertexConsumer {
    public var vertices : Array<Vec2>;
    public function new() {
        vertices = [];
    }
    public function add( x : Float, y : Float ) : Void {
        vertices.push([ x, y ]);
    }
    public function remove_all() : Void {
        vertices = [];
    }
}
