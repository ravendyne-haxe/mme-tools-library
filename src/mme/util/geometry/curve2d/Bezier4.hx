package mme.util.geometry.curve2d;

import mme.math.glmatrix.Vec2;

import mme.util.geometry.AggMath;


// http://antigrain.com/__code/include/agg_curves.h.html#curve4_div
// http://antigrain.com/__code/src/agg_curves.cpp.html

// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/src/agg_curves.cpp
// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_curves.h
class Bezier4 {

    //------------------------------------------------------------------------
    private static var curve_distance_epsilon : Float           = 1e-30;
    private static var curve_collinearity_epsilon : Float       = 1e-30;
    private static var curve_angle_tolerance_epsilon : Float    = 0.01;
    private static var curve_recursion_limit : Int              = 32;

    //------------------------------------------------------------------------
    var m_approximation_scale : Float;
    var m_angle_tolerance : Float;

    var m_cusp_limit : Float;

    var m_distance_tolerance_square : Float;

    public function set_approximation_scale( value : Float ) {
        m_approximation_scale = value;
        m_distance_tolerance_square = 0.5 / m_approximation_scale;
        m_distance_tolerance_square *= m_distance_tolerance_square;
    }
    public function get_approximation_scale() { return m_approximation_scale; }

    public function set_angle_tolerance( value : Float ) { m_angle_tolerance = value; }
    public function get_angle_tolerance() { return m_angle_tolerance; }

    public function set_cusp_limit( value : Float ) { m_cusp_limit = value; }
    public function get_cusp_limit() { return m_cusp_limit; }

    //------------------------------------------------------------------------
    public function new()
    {
        set_approximation_scale( 1.0 );
        set_angle_tolerance( 0.0 );
        set_cusp_limit( 0.0 );
    }

    //------------------------------------------------------------------------
    public function bezier(vc : VertexConsumer, p1 : Vec2, p2 : Vec2, p3 : Vec2, p4 : Vec2)
    {
        vc.remove_all();

        vc.add(p1.x, p1.y);
        recursive_bezier(vc, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y, 0);
        vc.add(p4.x, p4.y);
    }

    //------------------------------------------------------------------------
    private function recursive_bezier(vc : VertexConsumer,
                                      x1 : Float, y1 : Float, 
                                      x2 : Float, y2 : Float, 
                                      x3 : Float, y3 : Float, 
                                      x4 : Float, y4 : Float,
                                      level : Int)
    {
        if(level > curve_recursion_limit) 
        {
            return;
        }

        // Calculate all the mid-points of the line segments
        //----------------------
        var x12 : Float   = (x1 + x2) / 2;
        var y12 : Float   = (y1 + y2) / 2;
        var x23 : Float   = (x2 + x3) / 2;
        var y23 : Float   = (y2 + y3) / 2;
        var x34 : Float   = (x3 + x4) / 2;
        var y34 : Float   = (y3 + y4) / 2;
        var x123 : Float  = (x12 + x23) / 2;
        var y123 : Float  = (y12 + y23) / 2;
        var x234 : Float  = (x23 + x34) / 2;
        var y234 : Float  = (y23 + y34) / 2;
        var x1234 : Float = (x123 + x234) / 2;
        var y1234 : Float = (y123 + y234) / 2;


        // Try to approximate the full cubic curve by a single straight line
        //------------------
        var dx : Float = x4-x1;
        var dy : Float = y4-y1;

        var d2 : Float = Math.abs(((x2 - x4) * dy - (y2 - y4) * dx));
        var d3 : Float = Math.abs(((x3 - x4) * dy - (y3 - y4) * dx));
        var da1 : Float;
        var da2 : Float;
        var k   : Float;

        // switch((int(d2 > curve_collinearity_epsilon) << 1) +
        //         int(d3 > curve_collinearity_epsilon))
        var condition : Int = 0;
        if(d2 > curve_collinearity_epsilon) condition = 2;
        if(d3 > curve_collinearity_epsilon) condition += 1;
        switch(condition)
        {
        case 0:
            // All collinear OR p1==p4
            //----------------------
            k = dx*dx + dy*dy;
            if(k == 0)
            {
                d2 = AggMath.calc_sq_distance(x1, y1, x2, y2);
                d3 = AggMath.calc_sq_distance(x4, y4, x3, y3);
            }
            else
            {
                k   = 1 / k;
                da1 = x2 - x1;
                da2 = y2 - y1;
                d2  = k * (da1*dx + da2*dy);
                da1 = x3 - x1;
                da2 = y3 - y1;
                d3  = k * (da1*dx + da2*dy);
                if(d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1)
                {
                    // Simple collinear case, 1---2---3---4
                    // We can leave just two endpoints
                    return;
                }
                     if(d2 <= 0) d2 = AggMath.calc_sq_distance(x2, y2, x1, y1);
                else if(d2 >= 1) d2 = AggMath.calc_sq_distance(x2, y2, x4, y4);
                else             d2 = AggMath.calc_sq_distance(x2, y2, x1 + d2*dx, y1 + d2*dy);

                     if(d3 <= 0) d3 = AggMath.calc_sq_distance(x3, y3, x1, y1);
                else if(d3 >= 1) d3 = AggMath.calc_sq_distance(x3, y3, x4, y4);
                else             d3 = AggMath.calc_sq_distance(x3, y3, x1 + d3*dx, y1 + d3*dy);
            }
            if(d2 > d3)
            {
                if(d2 < m_distance_tolerance_square)
                {
                    vc.add(x2, y2);
                    return;
                }
            }
            else
            {
                if(d3 < m_distance_tolerance_square)
                {
                    vc.add(x3, y3);
                    return;
                }
            }


        case 1:
            // p1,p2,p4 are collinear, p3 is significant
            //----------------------
            if(d3 * d3 <= m_distance_tolerance_square * (dx*dx + dy*dy))
            {
                if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                {
                    vc.add(x23, y23);
                    return;
                }

                // Angle Condition
                //----------------------
                da1 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - Math.atan2(y3 - y2, x3 - x2));
                if(da1 >= Math.PI) da1 = 2*Math.PI - da1;

                if(da1 < m_angle_tolerance)
                {
                    vc.add(x2, y2);
                    vc.add(x3, y3);
                    return;
                }

                if(m_cusp_limit != 0.0)
                {
                    if(da1 > m_cusp_limit)
                    {
                        vc.add(x3, y3);
                        return;
                    }
                }
            }


        case 2:
            // p1,p3,p4 are collinear, p2 is significant
            //----------------------
            if(d2 * d2 <= m_distance_tolerance_square * (dx*dx + dy*dy))
            {
                if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                {
                    vc.add(x23, y23);
                    return;
                }

                // Angle Condition
                //----------------------
                da1 = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                if(da1 >= Math.PI) da1 = 2*Math.PI - da1;

                if(da1 < m_angle_tolerance)
                {
                    vc.add(x2, y2);
                    vc.add(x3, y3);
                    return;
                }

                if(m_cusp_limit != 0.0)
                {
                    if(da1 > m_cusp_limit)
                    {
                        vc.add(x2, y2);
                        return;
                    }
                }
            }


        case 3: 
            // Regular case
            //-----------------
            if((d2 + d3)*(d2 + d3) <= m_distance_tolerance_square * (dx*dx + dy*dy))
            {
                // If the curvature doesn't exceed the distance_tolerance value
                // we tend to finish subdivisions.
                //----------------------
                if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                {
                    vc.add(x23, y23);
                    return;
                }

                // Angle & Cusp Condition
                //----------------------
                k   = Math.atan2(y3 - y2, x3 - x2);
                da1 = Math.abs(k - Math.atan2(y2 - y1, x2 - x1));
                da2 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - k);
                if(da1 >= Math.PI) da1 = 2*Math.PI - da1;
                if(da2 >= Math.PI) da2 = 2*Math.PI - da2;

                if(da1 + da2 < m_angle_tolerance)
                {
                    // Finally we can stop the recursion
                    //----------------------
                    vc.add(x23, y23);
                    return;
                }

                if(m_cusp_limit != 0.0)
                {
                    if(da1 > m_cusp_limit)
                    {
                        vc.add(x2, y2);
                        return;
                    }

                    if(da2 > m_cusp_limit)
                    {
                        vc.add(x3, y3);
                        return;
                    }
                }
            }

        }

        // Continue subdivision
        //----------------------
        recursive_bezier(vc, x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1); 
        recursive_bezier(vc, x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1); 
    }
}
