package mme.util.geometry.curve2d;

import mme.math.glmatrix.Vec2;

import mme.util.geometry.AggMath;


// http://antigrain.com/__code/include/agg_curves.h.html#curve3_div
// http://antigrain.com/__code/src/agg_curves.cpp.html

// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/src/agg_curves.cpp
// https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_curves.h
class Bezier3 {

    //------------------------------------------------------------------------
    private static var curve_distance_epsilon : Float           = 1e-30;
    private static var curve_collinearity_epsilon : Float       = 1e-30;
    private static var curve_angle_tolerance_epsilon : Float    = 0.01;
    private static var curve_recursion_limit : Int              = 32;

    //------------------------------------------------------------------------
    var m_approximation_scale : Float;
    var m_angle_tolerance : Float;

    var m_distance_tolerance_square : Float;

    public function set_approximation_scale( value : Float ) {
        m_approximation_scale = value;
        m_distance_tolerance_square = 0.5 / m_approximation_scale;
        m_distance_tolerance_square *= m_distance_tolerance_square;
    }
    public function get_approximation_scale() { return m_approximation_scale; }

    public function set_angle_tolerance( value : Float ) { m_angle_tolerance = value; }
    public function get_angle_tolerance() { return m_angle_tolerance; }


    //------------------------------------------------------------------------
    public function new() 
    {
        set_approximation_scale( 1.0 );
        set_angle_tolerance( 0.0 );
    }

    //------------------------------------------------------------------------
    public function bezier(vc : VertexConsumer, p1 : Vec2, p2 : Vec2, p3 : Vec2)
    {
        vc.remove_all();

        vc.add(p1.x, p1.y);
        recursive_bezier(vc, p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, 0);
        vc.add(p3.x, p3.y);
    }

    //------------------------------------------------------------------------
    private function recursive_bezier(vc : VertexConsumer,
                                      x1 : Float, y1 : Float, 
                                      x2 : Float, y2 : Float, 
                                      x3 : Float, y3 : Float,
                                      level : Int)
    {
        if(level > curve_recursion_limit) 
        {
            return;
        }

        // Calculate all the mid-points of the line segments
        //----------------------
        var x12 : Float   = (x1 + x2) / 2;                
        var y12 : Float   = (y1 + y2) / 2;
        var x23 : Float   = (x2 + x3) / 2;
        var y23 : Float   = (y2 + y3) / 2;
        var x123 : Float  = (x12 + x23) / 2;
        var y123 : Float  = (y12 + y23) / 2;

        var dx : Float = x3-x1;
        var dy : Float = y3-y1;
        var d : Float = Math.abs(((x2 - x3) * dy - (y2 - y3) * dx));
        var da : Float;

        if(d > curve_collinearity_epsilon)
        { 
            // Regular case
            //-----------------
            if(d * d <= m_distance_tolerance_square * (dx*dx + dy*dy))
            {
                // If the curvature doesn't exceed the distance_tolerance value
                // we tend to finish subdivisions.
                //----------------------
                if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                {
                    vc.add(x123, y123);
                    return;
                }

                // Angle & Cusp Condition
                //----------------------
                da = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                if(da >= Math.PI) da = 2*Math.PI - da;

                if(da < m_angle_tolerance)
                {
                    // Finally we can stop the recursion
                    //----------------------
                    vc.add(x123, y123);
                    return;                 
                }
            }
        }
        else
        {
            // Collinear case
            //------------------
            da = dx*dx + dy*dy;
            if(da == 0)
            {
                d = AggMath.calc_sq_distance(x1, y1, x2, y2);
            }
            else
            {
                d = ((x2 - x1)*dx + (y2 - y1)*dy) / da;
                if(d > 0 && d < 1)
                {
                    // Simple collinear case, 1---2---3
                    // We can leave just two endpoints
                    return;
                }
                     if(d <= 0) d = AggMath.calc_sq_distance(x2, y2, x1, y1);
                else if(d >= 1) d = AggMath.calc_sq_distance(x2, y2, x3, y3);
                else            d = AggMath.calc_sq_distance(x2, y2, x1 + d*dx, y1 + d*dy);
            }
            if(d < m_distance_tolerance_square)
            {
                vc.add(x2, y2);
                return;
            }
        }

        // Continue subdivision
        //----------------------
        recursive_bezier(vc, x1, y1, x12, y12, x123, y123, level + 1); 
        recursive_bezier(vc, x123, y123, x23, y23, x3, y3, level + 1); 
    }
}