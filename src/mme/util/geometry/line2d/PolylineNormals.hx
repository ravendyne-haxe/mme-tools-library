package mme.util.geometry.line2d;

import mme.math.glmatrix.Vec2;

import mme.util.geometry.line2d.PolylineMiterUtil;

using mme.math.glmatrix.Vec2Tools;

class NormalsAttrib {
    public var norm : Vec2;
    public var len : Float;
    public function new( n : Vec2, l : Float ) {
        norm = n;
        len = l;
    }
    public function toString() : String {
        return '{ norm -> ' + norm.str() + ', len -> ' + len + ' }';
    }
}

class PolylineNormals {

    public static function getNormals( points : Array< Vec2 >, closed : Bool ) : Array< NormalsAttrib > {

        var lineA = new Vec2();
        var lineB = new Vec2();
        var tangent = new Vec2();
        var miter = new Vec2();


        var curNormal : Vec2 = null;
        var out : Array< NormalsAttrib > = [];

        if( closed ) {
            points = points.copy();
            points.push(points[0]);
        }


        var total = points.length;

        for ( idx in 1...total ) {

            var prev = points[ idx - 1 ];
            var curr = points[ idx ];
            var next : Vec2 = idx < points.length - 1 ? points[ idx + 1 ] : null;

            // find unit vector from prev to curr
            PolylineMiterUtil.unitDirection( lineA, prev, curr );


            if( idx == 1 ) {

                // the first normal
                curNormal = new Vec2();

                // first normal is always a perpendicular vector to lineA in CCW direction
                // since lineA is unit vector, we only have to rotate it 90 deg CCW
                // (positive dir in right-hand side coords)
                PolylineMiterUtil.perpendicularCCW( curNormal, lineA );

                // this is the first point we are working
                // with in this polyline, so we need to
                // add at this point a normal vector for
                // the point that comes before it, the point
                // with index == 0

                addNext( out, curNormal, 1.0 );
            }

            if( next == null ) {

                // this means we are working with the last point
                // in this polyline segment
                // we don't need to calculate anything special, just
                // add a normal that is perpendicular to lineA segment
                // in CCW direction
                PolylineMiterUtil.perpendicularCCW( curNormal, lineA );

                addNext( out, curNormal, 1.0 );

            } else {
                // this is the case where we have next point set,
                // so it means we are working with one of the points
                // that is between two other points.
                // Now we need to do a bit more work to find normal
                // in this point, which depends on where the other two points are

                // find unit vector from curr point to next
                PolylineMiterUtil.unitDirection( lineB, curr, next );

                // computeMiter() calculates tangent *unit* vector in curr point.
                // it also calculates miter *unit* vector in curr point, which should be perpendicular
                // to tangent vector in CCW direction.
                // miterLen is distance along miter vector from curr point to where offset line
                // meets a line defined by miter vector.
                var miterLen = PolylineMiterUtil.computeMiter( tangent, miter, lineA, lineB, 1.0 );
                addNext( out, miter, miterLen );
            }

        }

        //if the polyline is a closed loop, clean up the prev normal
        if( points.length > 2 && closed ) {

            // last point is NOT points[ total - 1 ]
            // because we are copying point[ 0 ] to the end of points[]
            // array when we work with closed loops
            var last2 = points[ total - 2 ];
            // first point
            var cur2 = points[ 0 ];
            // second point
            var next2 = points[ 1 ];

            PolylineMiterUtil.unitDirection( lineA, last2, cur2 );
            PolylineMiterUtil.unitDirection( lineB, cur2, next2 );
            PolylineMiterUtil.perpendicularCCW( curNormal, lineA );
            
            var miterLen2 = PolylineMiterUtil.computeMiter( tangent, miter, lineA, lineB, 1.0 );

            // update miter data on the first point
            out[ 0 ].norm = miter.clone();
            // out[ total - 1 ].norm = miter.clone();
            out[ 0 ].len = miterLen2;
            // out[ total - 1 ].len = miterLen2;

            // remove the point[0] copy from the back of array
            out.pop();
        }

        return out;
    }

    private static function addNext( out : Array< NormalsAttrib >, normal : Vec2, length : Float ) {

        out.push( new NormalsAttrib( normal.clone(), length ) );
    }
}
