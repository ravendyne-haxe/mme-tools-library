package mme.util.geometry.line2d;

import mme.math.glmatrix.Vec2;
using mme.math.glmatrix.Vec2Tools;

class PolylineMiterUtil {

    /**
    * Creates tangent and miter vectors for a point where two line segments
    * `lineA` and `lineB` meet. `halfThick` is amount of miter line offset from line segments
    * to either side. Both line segment parameters have to be **unit** vectors.
    *
    * @param {Vec2} resulting tangent unit vector
    * @param {Vec2} resulting miter unit vector
    * @param {Vec2} first line segment **unit** vector
    * @param {Vec2} second, consecutive, line segment **unit** vector
    * @param {Float} a vector specifying end point
    * @returns {Float} miter length
    */
    public static function computeMiter( tangent : Vec2, miter : Vec2, lineA : Vec2, lineB : Vec2, halfThick : Float ) : Float {

        // for two consecutive line segments, tangent is parallel to a line
        // that starts at start point of first segment and ends at end point
        // of the second one.
        lineA.add( lineB, tangent );
        // unit vector of the lineA + lineB is our tangent vector
        // for point where the line segments meet
        tangent.normalize( tangent );

        var tmp = new Vec2();

        // miter is perpendicular to tangent
        perpendicularCCW( miter, tangent );

        // length of miter is equal to the outline offset (`halfThick`) value
        // divided by cosine of angle between miter unit vector and
        // a vector that is normal to either lineA or lineB.
        // We chose lineA just because.

        // Get vector that is normal to lineA
        perpendicularCCW( tmp, lineA );

        // divide offset with cosine of the angle
        // since both tmp and lineA are unit vectors,
        // cosine is simply equal to their dot product
        return halfThick / miter.dot(tmp);
    }

    public static function perpendicularCCW( out : Vec2, dir : Vec2 ) : Vec2 {

        out.set( -dir[1], dir[0] );

        return out;
    }

    public static function perpendicularCW( out : Vec2, dir : Vec2 ) : Vec2 {

        out.set( dir[1], -dir[0]);

        return out;
    }

    /**
    * Creates a new unit-vector that has direction parallel to
    * line segment starting at point `a` and ending at point `b`
    *
    * @param {Vec2} a vector to store result to
    * @param {Vec2} a vector specifying start point
    * @param {Vec2} a vector specifying end point
    * @returns {Vec2} a unit 2D vector
    */
    public static function unitDirection( out : Vec2, a : Vec2, b : Vec2 ) : Vec2 {

        b.sub( a, out );
        out.normalize( out );

        return out;
    }
}
