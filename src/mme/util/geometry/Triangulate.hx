package mme.util.geometry;

import haxe.ds.Vector;
import mme.math.glmatrix.GLMatrix;
import mme.math.glmatrix.Vec2;


// http://www.flipcode.com/archives/Efficient_Polygon_Triangulation.shtml
class Triangulate {

    /**
        Triangulates a [simple polygon](https://en.wikipedia.org/wiki/Simple_polygon).
        If successful, `result` array will contain indexes into `contour` parameter array of vertices,
        each three consecutive values representing calculated triangles that triangulate the polygon.
        In other words: (`result[0]`, `result[1]`, `result[2]`) are indexes of vertices of the first triangle, etc.

        The algorithm does not support triangulating around holes. For those cases you'll have to divide the 2D shape
        into number of simple polygons which can then be triangulated separately by this function.

        @param contour an array of (X,Y) coordinates of vertices of the polygon to be triangulated
        @param result an array which will contain triangulation result
        @return `true` if triangulation was success, `false` otherwise
    **/
    public static function process( contour : Array<Vec2>, result : Array<Int>) : Bool {

        /* allocate and initialize list of Vertices in polygon */

        var n = contour.length;
        if ( n < 3 ) return false;

        var V = new Vector( n );

        /* we want a counter-clockwise polygon in V */
        if( 0.0 < area( contour ) )
            for( v in 0...n ) V[ v ] = v;
        else
            for( v in 0...n ) V[ v ] = ( n - 1 ) - v;

        var nv = n;

        /*  remove nv-2 Vertices, creating 1 triangle every time */
        var count : Int = 2 * nv;   /* error detection */

        var m = 0;
        var v = nv - 1;
        while( nv>2 )
        {
            /* if we loop, it is probably a non-simple polygon */
            if( 0 >= ( count-- ) )
            {
                //** Triangulate: ERROR - probable bad polygon!
                return false;
            }

            /* three consecutive vertices in current polygon, <u,v,w> */
            var u = v; if( nv <= u ) u = 0;     /* previous */
            v = u + 1; if( nv <= v ) v = 0;     /* new v    */
            var w = v + 1; if( nv <= w ) w = 0;     /* next     */

            if ( Snip( contour, u, v, w, nv, V ) )
            {
                var a,b,c,s,t;

                /* true names of the vertices */
                a = V[ u ]; b = V[ v ]; c = V[ w ];

                /* output Triangle */
                result.push( a );
                result.push( b );
                result.push( c );

                m++;

                /* remove v from remaining polygon */
                s = v;
                t = v + 1;
                while( t < nv ) {
                    V[ s ] = V[ t ];
                    s++;
                    t++;
                }
                nv--;

                /* resest error detection counter */
                count = 2 * nv;
            }
        }

        return true;
    }

    /**
        Compute area of contour/polygon. Area is positive for CCW polygons.

        @param contour array of (X,Y) vertices the polygon is made of
        @return area of the polygon
    **/
    public static function area( contour : Array<Vec2> ) : Float {

        var n : Int = contour.length;

        var A : Float =0.0;

        var p = n - 1;
        var q = 0;
        // for(int p=n-1,q=0; q<n; p=q++)
        while( q < n )
        {
            A += contour[ p ].x * contour[ q ].y - contour[ q ].x * contour[ p ].y;
            p = q;
            q++;
        }

        return A * 0.5;
    }

    /**
        Decide if point Px/Py is inside triangle defined by
        (Ax,Ay) (Bx,By) (Cx,Cy)
    **/
    public static function isInsideTriangle(
        Ax : Float, Ay : Float,
        Bx : Float, By : Float,
        Cx : Float, Cy : Float,
        Px : Float, Py : Float) : Bool {

        var ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
        var cCROSSap, bCROSScp, aCROSSbp;

        ax = Cx - Bx;  ay = Cy - By;
        bx = Ax - Cx;  by = Ay - Cy;
        cx = Bx - Ax;  cy = By - Ay;
        apx= Px - Ax;  apy= Py - Ay;
        bpx= Px - Bx;  bpy= Py - By;
        cpx= Px - Cx;  cpy= Py - Cy;

        aCROSSbp = ax * bpy - ay * bpx;
        cCROSSap = cx * apy - cy * apx;
        bCROSScp = bx * cpy - by * cpx;

        return ( ( aCROSSbp >= 0.0 ) && ( bCROSScp >= 0.0 ) && ( cCROSSap >= 0.0 ) );
    }

    static function Snip( contour : Array<Vec2>, u : Int, v : Int, w : Int, n : Int, V : Vector<Int> ) : Bool {

        var p : Int;
        var Ax, Ay, Bx, By, Cx, Cy, Px, Py;

        Ax = contour[ V[ u ] ].x;
        Ay = contour[ V[ u ] ].y;

        Bx = contour[ V[ v ] ].x;
        By = contour[ V[ v ] ].y;

        Cx = contour[ V[ w ] ].x;
        Cy = contour[ V[ w ] ].y;

        // static const float EPSILON=0.0000000001f; <-- original EPSILON = 1e-10, gl-matrix EPSILON = 1e-6
        if ( GLMatrix.EPSILON > ( ( ( Bx - Ax ) * ( Cy - Ay ) ) - ( ( By - Ay ) * ( Cx - Ax ) ) ) ) return false;

        for( p in 0...n )
        {
            if( ( p == u ) || ( p == v ) || ( p == w ) ) continue;
            Px = contour[ V [ p ] ].x;
            Py = contour[ V [ p ] ].y;
            if( isInsideTriangle( Ax, Ay, Bx, By, Cx, Cy, Px, Py ) ) return false;
        }

        return true;
    }

}