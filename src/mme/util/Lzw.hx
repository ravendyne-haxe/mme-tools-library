package mme.util;

import haxe.io.Bytes;

class Lzw {

    public static function compress( uncompressed : Bytes ) : Bytes {
        return uncompressed;
    }

    public static function decompress( compressed : Bytes ) : Bytes {
        return compressed;
    }
}
