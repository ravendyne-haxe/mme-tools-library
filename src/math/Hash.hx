package mme.math;

import haxe.io.Bytes;
import haxe.Int32;
import haxe.Int64;


class Hash {

    public static function fnv1aHash32( data : Bytes ) : Int32 {

        var fnvOffsetBasis : Int32 = 0x811c9dc5; // 2166136261
        var fnvPrime : Int32 = 0x1000193; // 16777619

        var hash : Int32 = fnvOffsetBasis;

        for( idx in 0...data.length ) {
            var c : Int32 = data.get( idx );
            hash = hash ^ c;
            hash = hash * fnvPrime;
        }

        return hash;
    }

    public static function fnv1aHash64( data : Bytes ) : Int64 {

        var fnvOffsetBasis : Int64 = Int64.make( 0xcbf29ce4, 0x84222325 ); // 14695981039346656037
        var fnvPrime : Int64 = Int64.make( 0x100, 0x000001b3 ); // 1099511628211

        var hash : Int64 = fnvOffsetBasis;

        for( idx in 0...data.length ) {
            var c : Int64 = data.get( idx );
            hash = hash ^ c;
            hash = hash * fnvPrime;
        }

        return hash;
    }

    public static function fnvHash32( val : String ) : Int32 {
        return fnv1aHash32( Bytes.ofString( val ) );
    }

    public static function fnvHash64( val : String ) : Int64 {
        return fnv1aHash64( Bytes.ofString( val ) );
    }
}
