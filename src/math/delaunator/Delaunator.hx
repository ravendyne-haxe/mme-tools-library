package mme.math.delaunator;

import mme.math.glmatrix.Vec2;

#if lime
import lime.utils.UInt32Array;
import lime.utils.Int32Array;
import lime.utils.Float64Array;
#else
import haxe.io.UInt32Array;
import haxe.io.Int32Array;
import haxe.io.Float64Array;
#end


/**
    Haxe port of [Mapbox Delaunator](https://mapbox.github.io/delaunator/).
**/
class Delaunator {

    public static function fill( array : Int32Array, value : Int ) {

    }

    public static var EPSILON ( default, never ) = Math.pow(2, -52);
    public static var EDGE_STACK = new UInt32Array(512);


    public var coords : Float64Array;

    public var triangles : UInt32Array;
    public var trianglesLen : Int;
    public var halfedges : Int32Array;
    var _hashSize : Int;
    var hull : UInt32Array;
    var hullPrev : UInt32Array;
    var hullNext : UInt32Array;
    var hullTri : UInt32Array;
    var hullStart : Int;

    var _cx : Float;
    var _cy : Float;


    public static function from( points : Array<Vec2> ) {
        var n = points.length;
        var coords = new Float64Array(n * 2);

        // for (let i = 0; i < n; i++) {
        for ( i in 0...n ) {
            var p = points[i];
            coords[2 * i] = p.x;
            coords[2 * i + 1] = p.y;
        }

        return new Delaunator(coords);
    }


    public function new( coords : Float64Array ) {
        doit( coords );
    }

    public function doit( coords : Float64Array ) {

        var n = coords.length >> 1;
        // if (n > 0 && typeof coords[0] != 'number') throw new Error('Expected coords to contain numbers.');

        this.coords = coords;

        // arrays that will store the triangulation graph
        var maxTriangles = 2 * n - 5;
        var triangles = this.triangles = new UInt32Array(maxTriangles * 3);
        var halfedges = this.halfedges = new Int32Array(maxTriangles * 3);

        // temporary arrays for tracking the edges of the advancing convex hull
        this._hashSize = Math.ceil(Math.sqrt(n));
        var hullPrev = this.hullPrev = new UInt32Array(n); // edge to prev edge
        var hullNext = this.hullNext = new UInt32Array(n); // edge to next edge
        var hullTri = this.hullTri = new UInt32Array(n); // edge to adjacent triangle
        // var hullHash = new Int32Array(this._hashSize).fill(-1); // angular edge hash
        var hullHash = new Int32Array(this._hashSize); // angular edge hash
        fill(hullHash, -1);

        // populate an array of point indices; calculate input data bbox
        var ids = new UInt32Array(n);
        var minX = Math.POSITIVE_INFINITY;
        var minY = Math.POSITIVE_INFINITY;
        var maxX = Math.NEGATIVE_INFINITY;
        var maxY = Math.NEGATIVE_INFINITY;

        for ( i in 0...n ) {
            var x = coords[2 * i];
            var y = coords[2 * i + 1];
            if (x < minX) minX = x;
            if (y < minY) minY = y;
            if (x > maxX) maxX = x;
            if (y > maxY) maxY = y;
            ids[i] = i;
        }
        var cx = (minX + maxX) / 2;
        var cy = (minY + maxY) / 2;

        var minDist = Math.POSITIVE_INFINITY;
        // var i0, i1, i2;
        var i0 = 0, i1 = 0, i2 = 0;

        // pick a seed point close to the center
        // for ( i in 0...n ) {
        for ( i in 0...n ) {
            var d = dist(cx, cy, coords[2 * i], coords[2 * i + 1]);
            if (d < minDist) {
                i0 = i;
                minDist = d;
            }
        }
        var i0x = coords[2 * i0];
        var i0y = coords[2 * i0 + 1];

        minDist = Math.POSITIVE_INFINITY;

        // find the point closest to the seed
        for ( i in 0...n ) {
            if (i == i0) continue;
            var d = dist(i0x, i0y, coords[2 * i], coords[2 * i + 1]);
            if (d < minDist && d > 0) {
                i1 = i;
                minDist = d;
            }
        }
        var i1x = coords[2 * i1];
        var i1y = coords[2 * i1 + 1];

        var minRadius = Math.POSITIVE_INFINITY;

        // find the third point which forms the smallest circumcircle with the first two
        for ( i in 0...n ) {
            if (i == i0 || i == i1) continue;
            var r = circumradius(i0x, i0y, i1x, i1y, coords[2 * i], coords[2 * i + 1]);
            if (r < minRadius) {
                i2 = i;
                minRadius = r;
            }
        }
        var i2x = coords[2 * i2];
        var i2y = coords[2 * i2 + 1];

        if (minRadius == Math.POSITIVE_INFINITY) {
            throw 'No Delaunay triangulation exists for this input.';
        }

        // swap the order of the seed points for counter-clockwise orientation
        if (orient(i0x, i0y, i1x, i1y, i2x, i2y)) {
            var i = i1;
            var x = i1x;
            var y = i1y;
            i1 = i2;
            i1x = i2x;
            i1y = i2y;
            i2 = i;
            i2x = x;
            i2y = y;
        }

        var center = circumcenter(i0x, i0y, i1x, i1y, i2x, i2y);
        this._cx = center.x;
        this._cy = center.y;

        var dists = new Float64Array(n);
        for ( i in 0...n ) {
            dists[i] = dist(coords[2 * i], coords[2 * i + 1], center.x, center.y);
        }

        // sort the points by distance from the seed triangle circumcenter
        quicksort(ids, dists, 0, n - 1);

        // set up the seed triangle as the starting hull
        this.hullStart = i0;
        var hullSize = 3;

        hullNext[i0] = hullPrev[i2] = i1;
        hullNext[i1] = hullPrev[i0] = i2;
        hullNext[i2] = hullPrev[i1] = i0;

        hullTri[i0] = 0;
        hullTri[i1] = 1;
        hullTri[i2] = 2;

        hullHash[this._hashKey(i0x, i0y)] = i0;
        hullHash[this._hashKey(i1x, i1y)] = i1;
        hullHash[this._hashKey(i2x, i2y)] = i2;

        this.trianglesLen = 0;
        this._addTriangle(i0, i1, i2, -1, -1, -1);

        // for (var k = 0, xp, yp; k < ids.length; k++) {
        var xp : Float = 0, yp : Float = 0;
        for ( k in 0...ids.length ) {
            var i = ids[k];
            var x = coords[2 * i];
            var y = coords[2 * i + 1];

            // skip near-duplicate points
            if (k > 0 && Math.abs(x - xp) <= EPSILON && Math.abs(y - yp) <= EPSILON) continue;
            xp = x;
            yp = y;

            // skip seed triangle points
            if (i == i0 || i == i1 || i == i2) continue;

            // find a visible edge on the convex hull using edge hash
            var start = 0;
            // for (var j = 0, key = this._hashKey(x, y); j < this._hashSize; j++) {
            var key = this._hashKey(x, y);
            for ( j in 0...this._hashSize ) {
                start = hullHash[(key + j) % this._hashSize];
                if (start != -1 && start != hullNext[start]) break;
            }

            start = hullPrev[start];
            var e = start;
            var q : Int;
            // while (q = hullNext[e], !orient(x, y, coords[2 * e], coords[2 * e + 1], coords[2 * q], coords[2 * q + 1])) {
                var q = hullNext[e];
            while (!orient(x, y, coords[2 * e], coords[2 * e + 1], coords[2 * q], coords[2 * q + 1])) {
                e = q;
                if (e == start) {
                    e = -1;
                    break;
                }
                q = hullNext[e];
            }
            if (e == -1) continue; // likely a near-duplicate point; skip it

            // add the first triangle from the point
            var t = this._addTriangle(e, i, hullNext[e], -1, -1, hullTri[e]);

            // recursively flip triangles from the point until they satisfy the Delaunay condition
            hullTri[i] = this._legalize(t + 2);
            hullTri[e] = t; // keep track of boundary triangles on the hull
            hullSize++;

            // walk forward through the hull, adding more triangles and flipping recursively
            var n = hullNext[e];
            // while (q = hullNext[n], orient(x, y, coords[2 * n], coords[2 * n + 1], coords[2 * q], coords[2 * q + 1])) {
                var q = hullNext[n];
            while (orient(x, y, coords[2 * n], coords[2 * n + 1], coords[2 * q], coords[2 * q + 1])) {
                t = this._addTriangle(n, i, q, hullTri[i], -1, hullTri[n]);
                hullTri[i] = this._legalize(t + 2);
                hullNext[n] = n; // mark as removed
                hullSize--;
                n = q;
                q = hullNext[n];
            }

            // walk backward from the other side, adding more triangles and flipping
            if (e == start) {
                // while (q = hullPrev[e], orient(x, y, coords[2 * q], coords[2 * q + 1], coords[2 * e], coords[2 * e + 1])) {
                    var q = hullPrev[e];
                while (orient(x, y, coords[2 * q], coords[2 * q + 1], coords[2 * e], coords[2 * e + 1])) {
                    t = this._addTriangle(q, i, e, -1, hullTri[e], hullTri[q]);
                    this._legalize(t + 2);
                    hullTri[q] = t;
                    hullNext[e] = e; // mark as removed
                    hullSize--;
                    e = q;
                    q = hullPrev[e];
                }
            }

            // update the hull indices
            this.hullStart = hullPrev[i] = e;
            hullNext[e] = hullPrev[n] = i;
            hullNext[i] = n;

            // save the two new edges in the hash table
            hullHash[this._hashKey(x, y)] = i;
            hullHash[this._hashKey(coords[2 * e], coords[2 * e + 1])] = e;
        }

        this.hull = new UInt32Array(hullSize);
        // for (var i = 0, e = this.hullStart; i < hullSize; i++) {
        var e = this.hullStart;
        for ( i in 0...hullSize ) {
            this.hull[i] = e;
            e = hullNext[e];
        }
        this.hullPrev = this.hullNext = this.hullTri = null; // get rid of temporary arrays

        // trim typed triangle mesh arrays
        this.triangles = triangles.subarray(0, this.trianglesLen);
        this.halfedges = halfedges.subarray(0, this.trianglesLen);
    }

    function _hashKey(x : Float, y : Float) {
        return Math.floor(pseudoAngle(x - this._cx, y - this._cy) * this._hashSize) % this._hashSize;
    }

    function _legalize(a) {
        // var {triangles, coords, halfedges} = this;
        var triangles = this.triangles;
        var coords = this.coords;
        var halfedges = this.halfedges;

        var i = 0;
        var ar = 0;

        // recursion eliminated with a fixed-size stack
        while (true) {
            var b = halfedges[a];

            /* if the pair of triangles doesn't satisfy the Delaunay condition
             * (p1 is inside the circumcircle of [p0, pl, pr]), flip them,
             * then do the same check/flip recursively for the new pair of triangles
             *
             *           pl                    pl
             *          /||\                  /  \
             *       al/ || \bl            al/    \a
             *        /  ||  \              /      \
             *       /  a||b  \    flip    /___ar___\
             *     p0\   ||   /p1   =>   p0\---bl---/p1
             *        \  ||  /              \      /
             *       ar\ || /br             b\    /br
             *          \||/                  \  /
             *           pr                    pr
             */
            var a0 = a - a % 3;
            ar = a0 + (a + 2) % 3;

            if (b == -1) { // convex hull edge
                if (i == 0) break;
                a = EDGE_STACK[--i];
                continue;
            }

            var b0 = b - b % 3;
            var al = a0 + (a + 1) % 3;
            var bl = b0 + (b + 2) % 3;

            var p0 = triangles[ar];
            var pr = triangles[a];
            var pl = triangles[al];
            var p1 = triangles[bl];

            var illegal = inCircle(
                coords[2 * p0], coords[2 * p0 + 1],
                coords[2 * pr], coords[2 * pr + 1],
                coords[2 * pl], coords[2 * pl + 1],
                coords[2 * p1], coords[2 * p1 + 1]);

            if (illegal) {
                triangles[a] = p1;
                triangles[b] = p0;

                var hbl = halfedges[bl];

                // edge swapped on the other side of the hull (rare); fix the halfedge reference
                if (hbl == -1) {
                    var e = this.hullStart;
                    do {
                        if (this.hullTri[e] == bl) {
                            this.hullTri[e] = a;
                            break;
                        }
                        e = this.hullNext[e];
                    } while (e != this.hullStart);
                }
                this._link(a, hbl);
                this._link(b, halfedges[ar]);
                this._link(ar, bl);

                var br = b0 + (b + 1) % 3;

                // don't worry about hitting the cap: it can only happen on extremely degenerate input
                if (i < EDGE_STACK.length) {
                    EDGE_STACK[i++] = br;
                }
            } else {
                if (i == 0) break;
                a = EDGE_STACK[--i];
            }
        }

        return ar;
    }

    private function _link(a, b) {
        this.halfedges[a] = b;
        if (b != -1) this.halfedges[b] = a;
    }

    // add a new triangle given vertex indices and adjacent half-edge ids
    private function _addTriangle(i0, i1, i2, a, b, c) {
        var t = this.trianglesLen;

        this.triangles[t] = i0;
        this.triangles[t + 1] = i1;
        this.triangles[t + 2] = i2;

        this._link(t, a);
        this._link(t + 1, b);
        this._link(t + 2, c);

        this.trianglesLen += 3;

        return t;
    }











// monotonically increases with real angle, but doesn't need expensive trigonometry
function pseudoAngle(dx : Float, dy : Float) {
    var p = dx / (Math.abs(dx) + Math.abs(dy));
    return (dy > 0 ? 3 - p : 1 + p) / 4; // [0..1]
}

function dist(ax : Float, ay : Float, bx : Float, by : Float) {
    var dx = ax - bx;
    var dy = ay - by;
    return dx * dx + dy * dy;
}

function orient(px : Float, py : Float, qx : Float, qy : Float, rx : Float, ry : Float) {
    return (qy - py) * (rx - qx) - (qx - px) * (ry - qy) < 0;
}

function inCircle(ax : Float, ay : Float, bx : Float, by : Float, cx : Float, cy : Float, px : Float, py : Float) {
    var dx = ax - px;
    var dy = ay - py;
    var ex = bx - px;
    var ey = by - py;
    var fx = cx - px;
    var fy = cy - py;

    var ap = dx * dx + dy * dy;
    var bp = ex * ex + ey * ey;
    var cp = fx * fx + fy * fy;

    return dx * (ey * cp - bp * fy) -
           dy * (ex * cp - bp * fx) +
           ap * (ex * fy - ey * fx) < 0;
}

function circumradius(ax : Float, ay : Float, bx : Float, by : Float, cx : Float, cy : Float) {
    var dx = bx - ax;
    var dy = by - ay;
    var ex = cx - ax;
    var ey = cy - ay;

    var bl = dx * dx + dy * dy;
    var cl = ex * ex + ey * ey;
    var d = 0.5 / (dx * ey - dy * ex);

    var x = (ey * bl - dy * cl) * d;
    var y = (dx * cl - ex * bl) * d;

    return x * x + y * y;
}

function circumcenter(ax : Float, ay : Float, bx : Float, by : Float, cx : Float, cy : Float) : Vec2 {
    var dx = bx - ax;
    var dy = by - ay;
    var ex = cx - ax;
    var ey = cy - ay;

    var bl = dx * dx + dy * dy;
    var cl = ex * ex + ey * ey;
    var d = 0.5 / (dx * ey - dy * ex);

    var x = ax + (ey * bl - dy * cl) * d;
    var y = ay + (dx * cl - ex * bl) * d;

    // return {x, y};
    return [x, y];
}

function quicksort(ids : UInt32Array, dists : Float64Array, left : Int, right : Int) {
    if (right - left <= 20) {
        // for (var i = left + 1; i <= right; i++) {
        for ( i in left + 1...right + 1 ) {
            var temp = ids[i];
            var tempDist = dists[temp];
            var j = i - 1;
            while (j >= left && dists[ids[j]] > tempDist) ids[j + 1] = ids[j--];
            ids[j + 1] = temp;
        }
    } else {
        var median = (left + right) >> 1;
        var i = left + 1;
        var j = right;
        swap(ids, median, i);
        if (dists[ids[left]] > dists[ids[right]]) swap(ids, left, right);
        if (dists[ids[i]] > dists[ids[right]]) swap(ids, i, right);
        if (dists[ids[left]] > dists[ids[i]]) swap(ids, left, i);

        var temp = ids[i];
        var tempDist = dists[temp];
        while (true) {
            do {i++;} while (dists[ids[i]] < tempDist);
            do {j--;} while (dists[ids[j]] > tempDist);
            if (j < i) break;
            swap(ids, i, j);
        }
        ids[left + 1] = ids[j];
        ids[j] = temp;

        if (right - i + 1 >= j - left) {
            quicksort(ids, dists, i, right);
            quicksort(ids, dists, left, j - 1);
        } else {
            quicksort(ids, dists, left, j - 1);
            quicksort(ids, dists, i, right);
        }
    }
}

function swap(arr : UInt32Array, i : Int, j : Int) {
    var tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
}

}
