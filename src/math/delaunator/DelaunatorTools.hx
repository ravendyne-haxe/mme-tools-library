package mme.math.delaunator;

import mme.math.glmatrix.Vec2;


/**
    Helper tools for Haxe port of [Mapbox Delaunator](https://mapbox.github.io/delaunator/).
**/
class DelaunatorTools {

    public static function edgesOfTriangle( t : Int ) { return [3 * t, 3 * t + 1, 3 * t + 2]; }
    public static function triangleOfEdge( e : Int )  { return Math.floor(e / 3); }
    public static function nextHalfedge( e : Int ) { return (e % 3 == 2) ? e - 2 : e + 1; }
    public static function prevHalfedge( e : Int ) { return (e % 3 == 0) ? e + 2 : e - 1; }

    public static function forEachTriangleEdge( delaunay : Delaunator, points : Array<Vec2>, callback : Int -> Vec2 -> Vec2 -> Void ) {
        // for (let e = 0; e < delaunay.triangles.length; e++) {
        for ( e in 0...delaunay.triangles.length ) {
            if (e > delaunay.halfedges[e]) {
                var p = points[delaunay.triangles[e]];
                var q = points[delaunay.triangles[nextHalfedge(e)]];
                callback(e, p, q);
            }
        }
    }

    public static function pointsOfTriangle( delaunay : Delaunator, t : Int ) : Array<Int> {
        return edgesOfTriangle(t)
            .map(function(e) return delaunay.triangles[e]);
    }

    public static function forEachTriangle( delaunay : Delaunator, points : Array<Vec2>, callback : Int -> Array<Vec2> -> Void ) {
        // for (let t = 0; t < delaunay.triangles.length / 3; t++) {
        for ( t in 0...Std.int(delaunay.triangles.length / 3) ) {
            callback( t, pointsOfTriangle(delaunay, t).map(function(p) return points[p]) );
        }
    }

    public static function trianglesAdjacentToTriangle( delaunay : Delaunator, t : Int ) {
        var adjacentTriangles = [];
        // for (var e of edgesOfTriangle(t)) {
        for ( e in edgesOfTriangle(t) ) {
            var opposite = delaunay.halfedges[e];
            if (opposite >= 0) {
                adjacentTriangles.push(triangleOfEdge(opposite));
            }
        }
        return adjacentTriangles;
    }

    public static function triangleCenter( delaunay : Delaunator, points : Array<Vec2>, t : Int ) : Vec2 {
        var vertices = pointsOfTriangle(delaunay, t).map(function(p) return points[p]);
        return circumcenter(vertices[0], vertices[1], vertices[2]);
    }

    public static function circumcenter(a : Vec2, b : Vec2, c : Vec2) : Vec2 {
        var ad = a[0] * a[0] + a[1] * a[1];
        var bd = b[0] * b[0] + b[1] * b[1];
        var cd = c[0] * c[0] + c[1] * c[1];
        var D = 2 * (a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]));
        return [
            1 / D * (ad * (b[1] - c[1]) + bd * (c[1] - a[1]) + cd * (a[1] - b[1])),
            1 / D * (ad * (c[0] - b[0]) + bd * (a[0] - c[0]) + cd * (b[0] - a[0])),
        ];
    }

    public static function forEachVoronoiEdge( delaunay : Delaunator, points : Array<Vec2>, callback : Int -> Vec2 -> Vec2 -> Void ) {
        // for (let e = 0; e < delaunay.triangles.length; e++) {
        for ( e in 0...delaunay.triangles.length ) {
            if (e < delaunay.halfedges[e]) {
                var p = triangleCenter(delaunay, points, triangleOfEdge(e));
                var q = triangleCenter(delaunay, points, triangleOfEdge(delaunay.halfedges[e]));
                callback(e, p, q);
            }
        }
    }

    public static function triangles( delaunay : Delaunator, points : Array<Vec2>, ?coordinates : Array<Array<Vec2>> ) {

        if( coordinates == null ) coordinates = [];

        var i = 0;
        while ( i < delaunay.triangles.length ) {
            coordinates.push([
                points[delaunay.triangles[i]],
                points[delaunay.triangles[i + 1]],
                points[delaunay.triangles[i + 2]]
            ]);
            i += 3;
        }

        return coordinates;
    }
}
