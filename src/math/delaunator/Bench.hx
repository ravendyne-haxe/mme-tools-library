package mme.math.delaunator;

import haxe.Timer;
import lime.utils.Log;

import mme.math.glmatrix.Vec2;


typedef BenchGenerator = {
    var name : String;
    var gen : Int -> Array<Vec2>;
}

class Bench {

    static function triangulate(points:Array<Vec2>) {
        Delaunator.from(points);
        // fasterDelaunay(points).triangulate();
        // voronoi()(points);
        // incrementalDelaunay(points);
        // delaunayFast.triangulate(points);
        // delaunaySlow.triangulate(points);
        // delaunayTriangulate(points);
        // cdt2d(points);
    }

    public static function doit() {

        var distributions : Array<BenchGenerator> = [
            {name: 'uniform', gen: uniform}, 
            {name: 'gaussian', gen: gaussian}, 
            {name: 'grid', gen: grid}, 
            {name: 'degenerate', gen: degenerate}];
        // var distributions : Array<Int -> Array<Vec2>> = [uniform, gaussian, grid, degenerate];
        var counts = [20000, 100000, 200000, 500000, 1000000];

        for (generate in distributions) {
            Log.info('${generate.name}:');

            // warmup
            Log.info('Warmup...');
            triangulate(generate.gen(counts[0]));
            triangulate(generate.gen(counts[1]));
            Log.info('Done.');

            for ( i in 0...counts.length ) {
                var c = counts[i];
                var points = generate.gen(c);

                var start = Timer.stamp();
                Log.info('# of points: ${c}');
                triangulate(points);

                var time = Timer.stamp() - start;
                var unit = 's';
                if( time < 1.0 ) {
                    time *= 1000;
                    unit = 'ms';
                }
                Log.info('time : ${time} ${unit}');
            }

            Log.info('Benchmark done.');
        }
    }

    static function uniform(count) {
        var points:Array<Vec2> = [];
        for ( i in 0...count ) {
            points.push([Math.random() * 1e3, Math.random() * 1e3]);
        }
        return points;
    }

    static function grid(count) {
        var points:Array<Vec2> = [];
        var size = Std.int(Math.sqrt(count));
        for ( i in 0...size ) {
            for ( j in 0...size ) {
                points.push([i, j]);
            }
        }
        return points;
    }

    static function gaussian(count) {
        var points:Array<Vec2> = [];
        for ( i in 0...count ) {
            points.push([pseudoNormal() * 1e3, pseudoNormal() * 1e3]);
        }
        return points;
    }

    static function degenerate(count) {
        var points:Array<Vec2> = [[0.0, 0.0]];
        for ( i in 0...count ) {
            var angle = 2 * Math.PI * i / count;
            points.push([1e10 * Math.sin(angle), 1e10 * Math.cos(angle)]);
        }
        return points;
    }

    static function pseudoNormal() {
        var v = Math.random() + Math.random() + Math.random() + Math.random() + Math.random() + Math.random();
        return Math.min(0.5 * (v - 3) / 3, 1);
    }
}
