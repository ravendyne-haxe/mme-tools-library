# Mini Mighty Engine Tools Library

Tools, classes and utilities used in [MME](https://gitlab.com/ravendyne-haxe/mini-mighty-engine), bundled together in their own library so they can be used without MME.

Dependencies:
- [haxe-gl-matrix](https://gitlab.com/ravendyne-haxe/haxe-gl-matrix)

## Contents

- File formats:
    - [Format](https://github.com/HaxeFoundation/format)-like classes for reading:
        - STL (binary)
        - OBJ (subset)
        - MTL (subset)
        - SVG Path `d` attribute string
    - reading/writing:
        - .fontatlas ([MME](https://gitlab.com/ravendyne-haxe/mini-mighty-engine) binary file for storing font atlas data)

- Math and related stuff
    - Quick-n-dirty port of [Mapbox Delaunator](https://mapbox.github.io/delaunator/) to Haxe
    - Haxe implementation of common easing functions, port of [AHEasing](https://github.com/warrenm/AHEasing/tree/master/AHEasing) code
    - [FNV-1a](https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function#FNV-1a_hash) 32-bit and 64-bit hash functions

- Geometry related
    - Port of AGG v2.4 quad and cubic [Bezier curve](https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_curves.h) generators
    - Functions to generate structures and data for drawing polyline outlines using OpenGL shaders. Based (partially) on [AGG 2.4 stroker class](https://sourceforge.net/p/agg/svn/HEAD/tree/agg-2.4/include/agg_math_stroke.h)
    - Classes and functions to parse SVG Path `d` attribute into `Path2D` commands, converting `Path2D` commands into array of line segments ready for rendering

- Utilities
    - Implementation of LZW algorithm
